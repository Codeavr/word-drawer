﻿using System.Linq;
using Codeavr.ArrayExtensions;
using Data.Sampled;
using UnityEngine;
using UnityEngine.EventSystems;
using Utilities;

public class StrokeDrawingValidator
{
    public delegate void DrawingValidationDelegate(StrokeDrawingValidator validator);

    public event DrawingValidationDelegate SucceedEvent;
    public event DrawingValidationDelegate FailedEvent;

    public bool CanDraw { get; set; }
    
    private readonly SampledStrokeData _stroke;
    private readonly float _curveDrawingEndDistance;
    private readonly float _drawingMaxError;
    private readonly CurveView[] _views;
    private readonly NodeView _startingNode;

    private int _currentCurveIndex;
    private bool _isDrawing;
    private bool _isDone;
    private Matrix4x4 _worldToLocalMatrix;

    public StrokeDrawingValidator
    (
        NodeView startingNode,
        Matrix4x4 worldToLocalMatrix,
        SampledStrokeData stroke,
        CurveView[] views,
        float curveDrawingEndDistance,
        float drawingMaxError
    )
    {
        _startingNode = startingNode;
        _worldToLocalMatrix = worldToLocalMatrix;
        _views = views;
        _drawingMaxError = drawingMaxError;
        _curveDrawingEndDistance = curveDrawingEndDistance;
        _stroke = stroke;

        _startingNode.DragStartedEvent += OnDragStarted;
        _startingNode.DraggingEvent += OnDrag;
        _startingNode.DragEndedEvent += OnDragEnded;
    }

    private void OnDragStarted(NodeView node, PointerEventData eventData)
    {
        if (!CanDraw) return;
        
        _isDrawing = true;

        Draw(GetPointerWorldPosition(eventData));
    }

    private void OnDrag(NodeView node, PointerEventData eventData)
    {
        if (!_isDrawing) return;

        Draw(GetPointerWorldPosition(eventData));
    }

    private void OnDragEnded(NodeView node, PointerEventData eventData)
    {
        if (!_isDone)
        {
            FailedEvent?.Invoke(this);
            Reset();
        }
        
        _isDrawing = false;
    }

    private void Draw(Vector2 drawPoint)
    {
        if (_isDone) return;

        (Vector2 point, float sqrDistance) closestPoint = (default, float.MaxValue);

        var sampledCurve = _stroke[_currentCurveIndex];
        foreach (var point in sampledCurve)
        {
            var sqrDistance = (drawPoint - point).sqrMagnitude;
            if (sqrDistance < closestPoint.sqrDistance)
            {
                closestPoint = (point, sqrDistance);
            }
        }

        if (Mathf.Sqrt(closestPoint.sqrDistance) >= _drawingMaxError)
        {
            SetInitialState();

            _isDrawing = false;

            FailedEvent?.Invoke(this);

            return;
        }

        var curve = _stroke[_currentCurveIndex];

        float t = CalculateNormalizedPositionOnCurve(curve, closestPoint.point);

        _views[_currentCurveIndex].FillingAmount = t;

        if (Vector2.Distance(_stroke[_currentCurveIndex].Last(), drawPoint) < _curveDrawingEndDistance)
        {
            if (_stroke.Count - 1 == _currentCurveIndex)
            {
                SetCompletedState();

                SucceedEvent?.Invoke(this);
            }
            else
            {
                _views[_currentCurveIndex].FillingAmount = 1f;
                _currentCurveIndex++;
            }
        }
    }

    public void Reset()
    {
        SetInitialState();
    }
    
    private void SetCompletedState()
    {
        _views.ForEach(v => v.FillingAmount = 1f);
        _isDone = true;
        _isDrawing = false;
    }

    private void SetInitialState()
    {
        _views.ForEach(v => v.FillingAmount = 0f);
        _currentCurveIndex = 0;
        _isDone = false;
        _isDrawing = false;
    }

    private Vector2 GetPointerWorldPosition(PointerEventData eventData)
    {
        var pointerWorldPosition = Camera.main.ScreenToWorldPoint(eventData.position);
        return _worldToLocalMatrix.MultiplyPoint(new Vector2(pointerWorldPosition.x, pointerWorldPosition.y));
    }

    private static float CalculateNormalizedPositionOnCurve(SampledCurveData curve, Vector2 closestPointPoint)
    {
        // TODO: calculation on non-linear curves
        return Vector2Utilities.InverseLerp(curve.First(), curve.Last(), closestPointPoint);
    }
}