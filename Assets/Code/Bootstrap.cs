﻿using System;
using System.Collections.Generic;
using System.Linq;
using Curves;
using Data;
using Serialization;
using UI;
using UnityEngine;

public class Bootstrap : MonoBehaviour
{
    [SerializeField]
    private ShapeDrawingController _shapeDrawerPrototype;

    private void Start()
    {
        var json = Json.Serialize(CreateTestData());

        var shape = Json.Deserialize<ShapeData>(json);

        var shapeDrawer = Instantiate(_shapeDrawerPrototype);
        shapeDrawer.Initialize(shape);

        FindObjectOfType<UiTryAgainLabel>().Resolve(shapeDrawer);
    }

    private ShapeData CreateTestData()
    {
        return new ShapeData
        (
            new StrokeData
            (
                new LinearCurve(new Vector2(0, 0), new Vector2(3, 9)),
                new LinearCurve(new Vector2(3, 9), new Vector2(6, 0))
            ),
            new StrokeData
            (
                new LinearCurve(new Vector2(1, 3), new Vector2(5, 3))
            )
        );
    }
}