﻿using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Assertions;

namespace Curves
{
    public class LinearCurve : IVectorCurve
    {
        [JsonProperty("Start")]
        private readonly Vector2 _start;
        [JsonProperty("End")]
        private readonly Vector2 _end;

        public LinearCurve(Vector2 start, Vector2 end)
        {
            _end = end;
            _start = start;
        }

        Vector2[] IVectorCurve.GetControlPoints()
        {
            return new[] {_start, _end};
        }

        Vector2 IVectorCurve.Sample(float t)
        {
            Assert.IsTrue(t >= 0);
            Assert.IsTrue(t <= 1);

            return Vector2.Lerp(_start, _end, t);
        }
    }
}