﻿using System.Collections.Generic;
using UnityEngine;

namespace Curves
{
    public class CurveSampler
    {
        private float _step;
        private float _samplesMinDistance;


        public CurveSampler(float step, float samplesMinMinDistance)
        {
            _samplesMinDistance = samplesMinMinDistance;
            _step = step;
        }

        public IEnumerable<Vector2> SampleCurve(IVectorCurve curve)
        {
            float sqrMinDistance = _samplesMinDistance;

            var lastSample = curve.Sample(0);

            yield return lastSample;

            float currentT = _step;

            while (currentT < 1f)
            {
                var sample = curve.Sample(currentT);

                if ((sample - lastSample).sqrMagnitude > sqrMinDistance)
                {
                    yield return sample;
                    lastSample = sample;
                }

                currentT += _step;
            }

            yield return curve.Sample(1f);
        }
    }
}