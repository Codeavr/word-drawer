﻿using UnityEngine;

namespace Curves
{
    public interface IVectorCurve
    {
        Vector2[] GetControlPoints();
        Vector2 Sample(float t);
    }
}