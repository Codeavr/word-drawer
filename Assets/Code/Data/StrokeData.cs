﻿using System.Collections.Generic;
using Curves;
using Newtonsoft.Json;

namespace Data
{
    public class StrokeData
    {
        [JsonIgnore] 
        public IReadOnlyList<IVectorCurve> Curves => _curves;

        [JsonProperty("Curves")]
        private IVectorCurve[] _curves;

        public StrokeData(params IVectorCurve[] curves)
        {
            _curves = curves;
        }
    }
}