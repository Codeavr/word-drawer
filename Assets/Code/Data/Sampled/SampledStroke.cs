﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Data.Sampled
{
    public class SampledStrokeData : IEnumerable<SampledCurveData>
    {
        public int Count => _sampledCurves.Length;

        private SampledCurveData[] _sampledCurves;

        public SampledStrokeData(params SampledCurveData[] sampledCurves)
        {
            _sampledCurves = sampledCurves;
        }

        public IEnumerator<SampledCurveData> GetEnumerator()
        {
            return _sampledCurves.AsEnumerable().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public SampledCurveData this[int index] => _sampledCurves[index];
    }
}