﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Data.Sampled
{
    public class SampledCurveData : IEnumerable<Vector2>
    {
        public int Count => _points.Length;

        private Vector2[] _points;

        public SampledCurveData(params Vector2[] points)
        {
            _points = points;
        }

        public IEnumerator<Vector2> GetEnumerator()
        {
            return _points.AsEnumerable().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public object this[int index] => _points[index];
    }
}