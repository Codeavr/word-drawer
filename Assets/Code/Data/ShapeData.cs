﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Data
{
    public class ShapeData
    {
        [JsonIgnore] 
        public IReadOnlyList<StrokeData> Strokes => _strokes;

        [JsonProperty("Strokes")]
        private StrokeData[] _strokes;

        public ShapeData(params StrokeData[] strokes)
        {
            _strokes = strokes;
        }
    }
}