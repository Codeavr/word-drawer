﻿using UnityEngine;

namespace Utilities
{
    public static class Vector2Utilities
    {
        public static float InverseLerp(Vector2 start, Vector2 end, Vector2 point)
        {
            var directionToEnd = end - start;
            var directionFromPoint = point - start;

            return Vector2.Dot(directionFromPoint, directionToEnd) / Vector2.Dot(directionToEnd, directionToEnd);
        }
    }
}