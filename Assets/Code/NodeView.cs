﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class NodeView : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public delegate void NodeDragDelegate(NodeView node, PointerEventData eventData);

    [SerializeField]
    private TMP_Text _text;

    [SerializeField]
    private Canvas _canvas;

    public event NodeDragDelegate DragStartedEvent;
    public event NodeDragDelegate DraggingEvent;
    public event NodeDragDelegate DragEndedEvent;

    public int Id { get; private set; } = -1;

    public void Initialize(int id)
    {
        Id = id;
        _canvas.worldCamera = Camera.main; // TODO: replace with camera injection

        _text.text = Id.ToString();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        DragStartedEvent?.Invoke(this, eventData);
    }

    void IEndDragHandler.OnEndDrag(PointerEventData eventData)
    {
        DragEndedEvent?.Invoke(this, eventData);
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        DraggingEvent?.Invoke(this, eventData);
    }
}