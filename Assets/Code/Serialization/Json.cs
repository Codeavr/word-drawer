﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.UnityConverters.Math;

namespace Serialization
{
    public static class Json
    {
        private static JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            Converters = new List<JsonConverter>
            {
                new Vector2Converter()
            },
            TypeNameHandling = TypeNameHandling.Auto
        };

        static Json()
        {
            JsonConvert.DefaultSettings = () => Settings;
        }

        public static T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static string Serialize<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}