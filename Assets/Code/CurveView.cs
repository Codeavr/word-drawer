﻿using System;
using UnityEngine;

public class CurveView : MonoBehaviour
{
    private static readonly int FillingPropertyId = Shader.PropertyToID("_Filling");

    [SerializeField]
    private LineRenderer _bottomRenderer;

    [SerializeField]
    private LineRenderer _overlayRenderer;

    [SerializeField]
    private float _animationTime = 5f;

    public float FillingAmount { get; set; }

    private float _materialValue;
    private float _currentAnimationVelocity;

    private void Start()
    {
        _materialValue = _overlayRenderer.material.GetFloat(FillingPropertyId);
    }

    public void Initialize(Vector3[] points)
    {
        _bottomRenderer.positionCount = points.Length;
        _bottomRenderer.SetPositions(points);

        _overlayRenderer.positionCount = points.Length;
        _overlayRenderer.SetPositions(points);
    }

    private void Update()
    {
        if (Mathf.Approximately(_materialValue, FillingAmount)) return;

        var nextValue = Mathf.SmoothDamp(_materialValue, FillingAmount, ref _currentAnimationVelocity, _animationTime);
        _overlayRenderer.material.SetFloat(FillingPropertyId, nextValue);
        _materialValue = nextValue;
    }
}