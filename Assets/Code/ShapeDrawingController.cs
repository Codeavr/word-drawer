﻿using System;
using System.Collections.Generic;
using System.Linq;
using Codeavr.ArrayExtensions;
using Curves;
using Data;
using Data.Sampled;
using UnityEngine;

public class ShapeDrawingController : MonoBehaviour
{
    [SerializeField]
    private CurveView _curveViewPrototype;

    [SerializeField]
    private NodeView _nodePrototype;

    [SerializeField]
    private float _maxError = 1f;

    public event Action ShapeDrawnSuccessfullyEvent;
    public event Action ShapeDrawFailedEvent;

    private ShapeData _shape;
    private CurveSampler _sampler;
    private SampledStrokeData[] _sampledShape;
    private List<NodeView> _nodes;
    private CurveView[][] _views;
    private StrokeDrawingValidator[] _drawingValidators;

    public void Initialize(ShapeData shape)
    {
        _shape = shape;
        _sampler = new CurveSampler(0.01f, 0.05f);

        SampleShape();
        CreateCurvesMeshes();
        CreateNodes();
        CreateStrokeValidators();
    }

    private void CreateStrokeValidators()
    {
        int nodeIndex = 0;
        _drawingValidators = new StrokeDrawingValidator[_sampledShape.Length];
        for (var strokeIndex = 0; strokeIndex < _drawingValidators.Length; strokeIndex++)
        {
            var validator = new StrokeDrawingValidator
            (
                _nodes[nodeIndex],
                transform.worldToLocalMatrix,
                _sampledShape[strokeIndex],
                _views[strokeIndex],
                _maxError,
                _maxError
            );

            validator.SucceedEvent += OnSucceedStrokeDrawn;
            validator.FailedEvent += OnFailedStroke;

            _drawingValidators[strokeIndex] = validator;
            nodeIndex += _sampledShape[strokeIndex].Count * 2 - 1;
        }

        _drawingValidators.First().CanDraw = true;
    }

    private void OnSucceedStrokeDrawn(StrokeDrawingValidator validator)
    {
        if (_drawingValidators.Last() == validator)
        {
            ShapeDrawnSuccessfullyEvent?.Invoke();
        }
        else
        {
            validator.CanDraw = false;
            _drawingValidators[_drawingValidators.IndexOf(validator) + 1].CanDraw = true;
        }
    }

    private void OnFailedStroke(StrokeDrawingValidator _)
    {
        _drawingValidators.ForEach(v => v.Reset());

        foreach (var validator in _drawingValidators)
        {
            validator.Reset();
            validator.CanDraw = false;
        }

        _drawingValidators.First().CanDraw = true;

        ShapeDrawFailedEvent?.Invoke();
    }

    private void SampleShape()
    {
        _sampledShape = _shape.Strokes
            .Select(stroke => new SampledStrokeData(stroke.Curves
                .Select(curve => new SampledCurveData(_sampler.SampleCurve(curve).ToArray())).ToArray()))
            .ToArray();
    }

    private void CreateCurvesMeshes()
    {
        _views = new CurveView[_sampledShape.Length][];

        for (var strokeIndex = 0; strokeIndex < _sampledShape.Length; strokeIndex++)
        {
            SampledStrokeData stroke = _sampledShape[strokeIndex];
            _views[strokeIndex] = new CurveView[stroke.Count];

            for (var curveIndex = 0; curveIndex < stroke.Count; curveIndex++)
            {
                SampledCurveData curve = stroke[curveIndex];

                var points3d = curve.Select(v2 => (Vector3) v2).ToArray();

                var curveView = Instantiate(_curveViewPrototype, transform);

                curveView.Initialize(points3d);

                _views[strokeIndex][curveIndex] = curveView;
            }
        }
    }

    private void CreateNodes()
    {
        _nodes = new List<NodeView>();

        int nextId = 1;

        foreach (var stroke in _shape.Strokes)
        {
            for (var i = 0; i < stroke.Curves.Count; i++)
            {
                CreateNode(stroke.Curves[i].Sample(0));

                if (stroke.Curves.Count - 1 == i)
                {
                    CreateNode(stroke.Curves[i].Sample(1));
                }
            }
        }

        void CreateNode(Vector2 point)
        {
            var node = Instantiate(_nodePrototype, transform);

            node.transform.localPosition = (Vector3) point + Vector3.back * 2f;

            node.Initialize(nextId);

            _nodes.Add(node);

            nextId++;
        }
    }
}