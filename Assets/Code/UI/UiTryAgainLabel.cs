﻿using System.Collections;
using UnityEngine;

namespace UI
{
    public class UiTryAgainLabel : MonoBehaviour
    {
        [SerializeField]
        private CanvasGroup _canvasGroup;

        [SerializeField]
        private float _animationTime;

        public void Resolve(ShapeDrawingController drawingController)
        {
            drawingController.ShapeDrawFailedEvent += OnFailed;
        }

        void Start()
        {
            _canvasGroup.alpha = 0f;
        }

        private void OnFailed()
        {
            StopAllCoroutines();
            StartCoroutine(ShowAnimation(1, 0));

            IEnumerator ShowAnimation(float startAlpha, float endAlpha)
            {
                _canvasGroup.alpha = startAlpha;

                float endTime = Time.time + _animationTime;

                while (Time.time < endTime)
                {
                    yield return null;

                    float t = 1f - Mathf.Clamp01((endTime - Time.time) / _animationTime);
                    _canvasGroup.alpha = Mathf.Lerp(startAlpha, endAlpha, t * t);
                }

                _canvasGroup.alpha = endAlpha;
            }
        }
    }
}